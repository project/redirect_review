<?php
/**
 * @file
 * File to implement BaseFieldDefinition for Redirect review.
 */

namespace Drupal\redirect_review;

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * A class for defining fields for redirect review.
 */
class RedirectReviewFieldDefinitions {
  /**
   * Get a list of field definitions to add to the redirect entity.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition[]
   *   A list of fields.
   */
  public static function getAll() {
    $fields = [];

    $fields['review_status_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Redirect review status code'))
      ->setTargetEntityTypeId('redirect')
      ->setName('review_status_code')
      //->setInitialValue(null)
      ->setProvider('redirect_review')
      ->setDescription(t('Redirect review status code.'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
      ));

    $fields['review_status_updated'] = BaseFieldDefinition::create('timestamp')
      //->setRequired(TRUE)
      ->setTargetEntityTypeId('redirect')
      ->setName('review_status_updated')
      //->setInitialValue(\Drupal::time()->getRequestTime())
      ->setProvider('redirect_review')
      ->setDescription(t('The last time the redirect review was updated.'))
      ->setLabel(t('Redirect review updated'));

    return $fields;
  }
}
