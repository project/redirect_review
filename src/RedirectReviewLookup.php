<?php
/**
 * @file
 * File to look for redirect issues as a batch job.
 */

namespace Drupal\redirect_review;

use Exception;

/**
 * Class to perform the review review through http requests.
 */
class RedirectReviewLookup {
  /**
   * Function to look for redirect issues through http requests.
   *
   * @param String $domain
   *   The domain on which the request should be made.
   * @param String $lang
   *   The language code of the path.
   * @param Integer $rid
   *   The redirect id from Redirects table.
   * @param String $uri
   *   The uri path that needs to be reviewed.
   * @param Array $context
   *   The context for this bactch job.
   */
  public static function lookForRedirectIssues($domain, $lang, $rid, $uri, &$context) {
    $client = \Drupal::httpClient();

    $message = "Validating redirects - $domain$lang$uri";

    $status_code = '';

    try {
      $client->request('GET', "$domain$lang$uri");
    }
    catch (Exception $e) {
      $status_code = $e->getCode();
    }

    // Review status code and updated time.
    $query = \Drupal::database()->update('redirect');
    $query->fields([
      'review_status_code' => $status_code,
      'review_status_updated' => \Drupal::time()->getRequestTime(),
    ]);
    $query->condition('rid', $rid);
    $query->execute();

    $context['message'] = $message;
    $context['results'][] = $rid;

    sleep(1);
  }
}